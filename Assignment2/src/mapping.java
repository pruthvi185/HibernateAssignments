
import java.util.*;
 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class mapping {
   private static SessionFactory factory; 
   public static void main(String[] args) {
      
      try {
         factory = new Configuration().configure().buildSessionFactory();
      } catch (Throwable ex) { 
         System.err.println("Failed to create sessionFactory object." + ex);
         throw new ExceptionInInitializerError(ex); 
      }
      
      mapping ME = new mapping();
     
      HashSet set1 = new HashSet();
      set1.add(new Employee("1","A","b1","11"));
      set1.add(new Employee("2","Av","b1","11"));
      set1.add(new Employee("5","Ax","b3","11"));
    
      String depID1 = ME.addDepartment("11","abc",set1);

 
      HashSet set2 = new HashSet();
      set2.add(new Employee("3","Av","b1","12"));
      set2.add(new Employee("4","Ax","b3","12"));

    
      String depID2 = ME.addDepartment("12","ss",set2);

      ME.listDepartment();

     
      ME.updateDepartment("11", "cse");

    
      ME.deleteDepartment("12");

      ME.listDepartment();

   }

   public String addDepartment(String deptid, String deptname, Set emp){
      Session session = factory.openSession();
      Transaction tx = null;
      String departmentID = null;
      
      try {
         tx = session.beginTransaction();
         Department department = new Department(deptid, deptname);
         department.setEmployee(emp);
         departmentID = (String) session.save(department); 
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
      return departmentID;
   }

   public void listDepartment( ){
      Session session = factory.openSession();
      Transaction tx = null;
      
      try {
         tx = session.beginTransaction();
         List department = session.createQuery("FROM Department").list(); 
         for (Iterator iterator1 = department.iterator(); iterator1.hasNext();){
            Department department1 = (Department) iterator1.next(); 
            System.out.print("Dept ID: " + department1.getDeptid()); 
            System.out.print(" Dept Name: " + department1.getDeptname()); 
           
            Set employee = department1.getEmployee();
            for (Iterator iterator2 = employee.iterator(); iterator2.hasNext();){
               Employee emp = (Employee) iterator2.next(); 
               System.out.println("Employee Id: " + emp.getEmpid()); 
               System.out.println("Employee name: " + emp.getEmpname()); 
               System.out.println("Employee Band: " + emp.getEmpband());
               System.out.println("Dept ID: " + emp.getDeptid()); 
            }
         }
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
   }
   
   
   public void updateDepartment(String Deptid, String deptname ){
      Session session = factory.openSession();
      Transaction tx = null;
      try {
         tx = session.beginTransaction();
         Department dep = (Department)session.get(Department.class, Deptid); 
         dep.setDeptname( deptname );
         session.update(dep);
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
   }
   
   
   public void deleteDepartment(String Deptid){
      Session session = factory.openSession();
      Transaction tx = null;
      
      try {
         tx = session.beginTransaction();
         Department dep = (Department)session.get(Department.class, Deptid); 
         session.delete(dep); 
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
   }
}