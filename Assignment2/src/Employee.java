

public class Employee {

	private String empid;
	private String empname;
	private String empband;
	private String Deptid;
	
	
	
	public Employee() {}
	   public Employee(String empid, String empname, String empband, String deptid) {
	      this.empid = empid;
	      this.empname = empname;
	      this.empband = empband;
	      this.Deptid= deptid;
	   }
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmpband() {
		return empband;
	}
	public void setEmpband(String empband) {
		this.empband = empband;
	}
	   public String getDeptid() {
			return Deptid;
		}
	   
		public void setDeptid(String deptid) {
			Deptid = deptid;
		}
	public boolean equals(Object obj) {
	      if (obj == null) return false;
	      if (!this.getClass().equals(obj.getClass())) return false;

	      Employee obj2 = (Employee)obj;
	      if((this.empid == obj2.getEmpid()) && (this.empname.equals(obj2.getEmpname()))) {
	         return true;
	      }
	      return false;
	   }
	   
	   public int hashCode() {
	      int tmp = 0;
	      tmp = ( empid + empname ).hashCode();
	      return tmp;
	   }
	
}

