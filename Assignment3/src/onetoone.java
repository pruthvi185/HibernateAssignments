
import java.util.*;
 
import org.hibernate.HibernateException; 
import org.hibernate.Session; 
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class onetoone {
   private static SessionFactory factory; 
   public static void main(String[] args) {
      
      try {
         factory = new Configuration().configure().buildSessionFactory();
      } catch (Throwable ex) { 
         System.err.println("Failed to create sessionFactory object." + ex);
         throw new ExceptionInInitializerError(ex); 
      }
      
      onetoone ME = new onetoone();

      
      Passport p1 = ME.addPassport("ab12ns","Hyderabad","1");

      /* Add employee records in the database */
     String empID1 = ME.addEmployee("1","Dhoni", "hyd","ab12ns",p1);

      /* Let us have another address object */
     Passport p2 = ME.addPassport("ab1w2xs","Mumbai","2");
  
      /* Add another employee record in the database */
     String empID2 = ME.addEmployee("2","kohli", "pune","ab1w2xs",p2);

      /* List down all the employees */
      ME.listEmployees();

      /* Update employee's salary records */
      ME.updateEmployee(empID1,"gnbcx");

      /* List down all the employees */
      ME.listEmployees();

   }

   /* Method to add an address record in the database */
   public Passport addPassport(String passid, String passname,String empid) {
      Session session = factory.openSession();
      Transaction tx = null;
      Integer passID = null;
      Passport p = null;
      
      try {
         tx = session.beginTransaction();
         p = new Passport(passid, passname, empid);
         passID = (Integer) session.save(p); 
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
      return p;
   }

   /* Method to add an employee record in the database */
   public String addEmployee(String empid, String empname, String empcity,String passid, Passport p){
      Session session = factory.openSession();
      Transaction tx = null;
      String employeeID = null;
      
      try {
         tx = session.beginTransaction();
         Employee employee = new Employee(empid, empname, empcity, passid, p);
         employeeID = (String) session.save(employee); 
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
      return employeeID;
   }

   /* Method to list all the employees detail */
   public void listEmployees( ){
      Session session = factory.openSession();
      Transaction tx = null;
      
      try {
         tx = session.beginTransaction();
         List employees = session.createQuery("FROM Employee").list(); 
         for (Iterator iterator = employees.iterator(); iterator.hasNext();){
            Employee employee = (Employee) iterator.next(); 
            System.out.print("Employee ID: " + employee.getEmpid()); 
            System.out.print("  Employee Name: " + employee.getEmpname()); 
            System.out.println("  Employee city: " + employee.getEmpcity());
            System.out.println("  Passport No: " + employee.getPassid());
            Passport pas = employee.getPass();
          
            System.out.println("  Passport No: " +  pas.getPassid());
            System.out.println("  Passportname: " + pas.getPassname());
            System.out.println(" Empid: " + pas.getEmpid());
           
         }
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
   }
   
   /* Method to update salary for an employee */
   public void updateEmployee(String EmployeeID, String passid ){
      Session session = factory.openSession();
      Transaction tx = null;
      
      try {
         tx = session.beginTransaction();
         Employee employee = (Employee)session.get(Employee.class, EmployeeID); 
         employee.setPassid( passid );
         session.update(employee);
         tx.commit();
      } catch (HibernateException e) {
         if (tx!=null) tx.rollback();
         e.printStackTrace(); 
      } finally {
         session.close(); 
      }
   }
}
