import java.util.*;
public class Employee {
   
	private String empid;
	private String empname;
    private String empcity;
    private String passid;
   

	private Passport pass; 
	

		public Employee() {}
	      
	      public Employee(String empid,String empname,String empcity,String passid,Passport pass){
	    	  
	    	  
	    	  this.empid= empid;
	    	  this.empname= empname;
	    	  this.empcity= empcity;
	    	  this.passid= passid;
	    	  this.pass= pass;
	    	  
	    			  
	      }
	      
	
	
	
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmpcity() {
		return empcity;
	}
	public void setEmpcity(String empcity) {
		this.empcity = empcity;
	}
	 public String getPassid() {
			return passid;
		}

		public void setPassid(String passid) {
			this.passid = passid;
		}

    public Passport getPass() {
	return pass;
   }

    public void setPass(Passport pass) {
	this.pass = pass;
    }
	
	
}
