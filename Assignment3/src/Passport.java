import java.util.*;

public class Passport {

	private String passid;
	private String passname;
	private String empid;
	 
	
	public Passport(){}
	public Passport(String passid,String passname, String empid){
		
		this.passid=passid;
		this.passname=passname;
		this.empid=empid;
		
	}
	public String getPassid() {
		return passid;
	}
	public void setPassid(String passid) {
		this.passid = passid;
	}
	public String getPassname() {
		return passname;
	}
	public void setPassname(String passname) {
		this.passname = passname;
	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	
	
	
}
