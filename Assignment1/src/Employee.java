

public class Employee {
    
	private String empid;
	private String empname;
	private String empband;
	
	 public Employee() {}
	   public Employee(String empid, String empname, String empband) {
	      this.empid = empid;
	      this.empname = empname;
	      this.empband = empband;
	   }
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getEmpname() {
		return empname;
	}
	public void setEmpname(String empname) {
		this.empname = empname;
	}
	public String getEmpband() {
		return empband;
	}
	public void setEmpband(String empband) {
		this.empband = empband;
	}
	
	
}
