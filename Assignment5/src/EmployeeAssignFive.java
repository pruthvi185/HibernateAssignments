
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name="employee_assignment_five")
@Inheritance(strategy=InheritanceType.JOINED)
public class EmployeeAssignFive {

	@Id
	@Column(name="employee_id")
	private int employeeId;
	
	@Column(name="name")
	private String employeeName;
	
	@Column(name="salary")
	private float salary;

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public float getSalary() {
		return salary;
	}

	public void setSalary(float salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EmployeeAssignFive : employeeId=").append(employeeId).append(", employeeName=")
				.append(employeeName).append(", salary=").append(salary);
		return builder.toString();
	}
	
}
